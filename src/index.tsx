import React from 'react';
import { render } from 'react-dom';
import { Provider as ModalProvider } from 'react-modaly';
import { ApolloProvider } from 'react-apollo';
import { ApolloProvider as ApolloClientProvider } from '@apollo/client';

// import { ApolloProvider as ApolloHooksProvider } from '@apollo/react-hooks'
import { InMemoryCache, ApolloClient } from 'apollo-boost';
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from 'apollo-link-context';
import { ApolloLink } from 'apollo-link';
import { GRAPHQL_ENDPOINT } from '../config';

import App from './App';

import './styles/index.scss';

const MODAL_NODE = document.getElementById('modal')!;
const ROOT_NODE = document.getElementById('root')!;

const httpLink = createUploadLink({
  uri: GRAPHQL_ENDPOINT,
});

const authLink = setContext((_, { headers }) => {
  const accessToken = localStorage.getItem('accessToken');

  return {
    headers: {
      ...headers,
      authorization: accessToken ? `Bearer ${accessToken}` : '',
    },
  };
});

const link = ApolloLink.from([
  authLink.concat(httpLink),
]);

const graphqlClient = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

render(
  <ApolloProvider client={graphqlClient}>
    <ApolloClientProvider client={graphqlClient}>
      <ModalProvider node={MODAL_NODE}>
        <App />
      </ModalProvider>
    </ApolloClientProvider>
  </ApolloProvider>,
  ROOT_NODE,
);
