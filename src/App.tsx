import React, { FunctionComponent } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Landing from "./components/landing/Landing";
import Admin from "./components/admin/Admin";


const App: FunctionComponent = (): JSX.Element => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Landing} />
            <Route path="/admin" component={Admin} />
        </Switch>
    </BrowserRouter>
);


export default App;
