import { withApollo } from 'react-apollo';
import React, { FunctionComponent, useCallback, useState } from 'react';
import { Modal, useDialog } from 'react-modaly';

import CartItem from './cartItem/CartItem';

import ProductDescription from '../dialogs/ProductDescription';
import ModalContainer from '../modal-container/ModalContainer';
import Buy from '../../modals/buy/Buy';

const DESCRIPTION = 'description';
const BUY = 'buy';

import { gql, useQuery } from '@apollo/client';

const GET_ITEMS = gql`
    query GetItemsForLanding {
        items(input: {activeFilter: {value: true}}) {
            id
            name
            description
            imageURL
        }
    }
`;

export interface ICart {
  id: string;
  name: string;
  description: string;
  imageURL: string;
}

const List: FunctionComponent = (): JSX.Element => {
  const { loading, error, data } = useQuery(GET_ITEMS);

  const { isOpen, open, close } = useDialog();

  const [selectedData, setSelectedData] = useState<ICart | null>(null);
  const [modalState, setModalState] = useState<string | null>(null);

  const handleClose = useCallback(() => {
    close();
  }, []);

  const handleSuccess = useCallback(() => {
    setModalState(BUY);
    open();
  }, []);

  const handleOpen = useCallback((data: ICart) => {
    setSelectedData(data);
    setModalState(DESCRIPTION);
    open();
  }, [selectedData]);

  const headerText: () => string = (): string => modalState === BUY ? 'Условия покупки' : '';

  const modal: () => JSX.Element = (): JSX.Element => {
    switch (modalState) {
      case BUY:
        return <Buy />;
      case DESCRIPTION:
        return (
          <ProductDescription
            onClose={close}
            onCancel={handleClose}
            onSuccess={handleSuccess}
            data={selectedData}
          />
        );
      default:
        return <></>;
    }
  };

  if (loading) {
    return <div>Загрузка...</div>;
  }

  if (error) {
    return <div>Сайт на техническом обслуживании.</div>;
  }

  const { items = [] } = data;

  return (
    <div className="List">
      <div className="List__description">
        <span className="description">Товары для питомников</span>
      </div>
      <div className="List__content">
        {
          items.map((item) => (
            <CartItem
              key={item.id}
              item={item}
              onMore={handleOpen}
            />
          ))
        }
      </div>
      <Modal isOpen={isOpen} as="section">
        <ModalContainer
          isOpen={isOpen}
          onClose={handleClose}
          headerText={headerText()}
        >
          {modal()}
        </ModalContainer>
      </Modal>
    </div>
  )
};

export default withApollo(List);
