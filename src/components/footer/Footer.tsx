import React, { FunctionComponent } from "react";


const Footer: FunctionComponent = (): JSX.Element => {
    return (
        <div className="Footer">
            <div className="Footer__wrapper">
                <div className="Footer__content">
                <span className="requisites">
                    ОГРН 1157746505256, ИНН 77213114888
                </span>
                </div>
                <div className="Footer__content">
                    <div className="phone">
                        8 (495) 642 41 83
                    </div>
                    <div className="email">
                        info@pitomniklsnab.ru
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Footer;
