import React, { FunctionComponent, useEffect, useState } from 'react';
import { withApollo } from 'react-apollo';
import { Admin as ReactAdmin, Resource } from 'react-admin';

import dataProviderFactory from '../../dataProvider';
import authProvider from '../../authProvider';
import Login from '../login/Login';
import Items from '../controls/Items';
import ItemCreate from '../controls/ItemCreate';
import ItemEdit from '../controls/ItemEdit';

const Admin: FunctionComponent = ({ client }): JSX.Element => {
  const [dataProvider, setDataProvider] = useState(null);

  useEffect(() => {
    let restoreFetch;

    const fetchDataProvider = async () => {
      const dataProviderInstance = await dataProviderFactory(client);

      setDataProvider(() => dataProviderInstance);
    };

    fetchDataProvider()
      .catch((error) => console.error(error));

    return restoreFetch;
  }, []);

  if (!dataProvider) {
    return (
      <div>Loading...</div>
    );
  }

  return (
    <ReactAdmin
      title=""
      loginPage={Login}
      authProvider={authProvider}
      dataProvider={dataProvider}
    >
      <Resource
        name="items"
        list={Items}
        create={ItemCreate}
        edit={ItemEdit}
      />
    </ReactAdmin>
  )
};

export default withApollo(Admin);
