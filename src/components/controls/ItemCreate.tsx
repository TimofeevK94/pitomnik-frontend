import * as React from 'react';
import { PropsWithChildren } from 'react';
import { Create, SimpleForm, TextInput, BooleanInput, ImageField, ImageInput, required } from 'react-admin';
import { useRefresh, useRedirect } from 'react-admin';

import RichTextInput from 'ra-input-rich-text';

export default (props: PropsWithChildren<{}>) => {
  const refresh = useRefresh();
  const redirect = useRedirect();

  const onSuccess = ({ data }) => {
    redirect('/items');
    refresh();
  };

  return (
    <Create onSuccess={onSuccess} {...props}>
      <SimpleForm>
        <TextInput source="name" label="Название" validate={[required('Заполните поле')]} />
        <RichTextInput source="description" label="Описание"  defaultValue=""/>
        <BooleanInput source="active" label="Показывать на сайте" defaultValue={false} />
        <ImageInput
          source="file"
          label="Картинка"
          accept=".jpg, .jpeg, .png"
          maxSize={20000000}
          validate={[required('Картинка обязательна')]}
        >
          <ImageField source="src" />
        </ImageInput>
      </SimpleForm>
    </Create>
  );
};


