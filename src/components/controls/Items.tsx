import React from 'react';
import {
  Datagrid, EditButton, List, TextField, ImageField, BooleanField, RichTextField,
} from 'react-admin';

export default ({ ...props }) => (
  <List
    {...props}
    sort={{ field: 'id', order: 'DESC' }}
    pagination={false}
    component="div"
  >
    <Datagrid>
      <TextField source="id" label="№" />
      <TextField source="name" label="Название" sortable={false} />
      <RichTextField source="description" label="Описание" sortable={false} />
      <ImageField source="imageURL" label="Картинка" sortable={false} />
      <BooleanField source="active" label="Показывать на сайте" />
      <EditButton label="Редактировать" />
    </Datagrid>
  </List>
);
