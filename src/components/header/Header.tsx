import React, { FunctionComponent, useCallback } from 'react';
import { Modal, useDialog } from 'react-modaly';

import LogoSvg from '../../assets/LogoSvg';
import ModalContainer from '../modal-container/ModalContainer';
import Buy from '../../modals/buy/Buy';

const Header: FunctionComponent = (): JSX.Element => {
  const { isOpen, open, close } = useDialog();

  const handleClose = useCallback(() => {
    close();
  }, []);

  const handleOpen = useCallback(() => {
    open();
  }, []);

  return (
    <div className="Header">
      <div className="Header__mainInfo">
        <div className="Header__content">
          <LogoSvg
            width="354"
            height="64"
          />
        </div>
        <div className="Header__content">
          <div className="Header__buy">
            <div className="Header__buy-block">
              <div className="Header__desc">
                <span className="question" onClick={handleOpen}>Как купить?</span>
              </div>
              <div className="Header__phone">
                <span className="phone">8 (495) 642 41 83</span>
              </div>
            </div>
          </div>
          <div className="Header__email">
            <div className="Header__buy">
              <span className="email">info@pitomniklsnab.ru</span>
            </div>
          </div>
        </div>
      </div>
      <div className="Header__service">
        <span className="desc-service">Продажа пластиковых технологических контейнеров от&nbsp;производителя</span>
      </div>
      <Modal isOpen={isOpen} as="section">
        <ModalContainer isOpen={isOpen} onClose={handleClose} headerText="Условия покупки">
          <Buy />
        </ModalContainer>
      </Modal>
    </div>
  )
}

export default Header;
