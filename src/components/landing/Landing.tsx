import React, { Fragment, FunctionComponent } from 'react';

import Header from '../header/Header';
import List from '../list/List';
import Footer from '../footer/Footer';

const Landing: FunctionComponent = (): JSX.Element => (
  <Fragment>
    <main>
      <Header />
      <List />
    </main>
    <Footer />
  </Fragment>
);

export default Landing;
