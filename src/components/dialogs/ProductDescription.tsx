import React, { FunctionComponent, useCallback } from 'react';
import { ICart } from '../list/List';

interface IProductDescriptionProps {
  onSuccess: () => void;
  onCancel: () => void;
  onClose: () => void;
  data: ICart;
}

const ProductDescription: FunctionComponent<IProductDescriptionProps> = ({ onSuccess, data }): JSX.Element => {
  const success = useCallback(() => {
    onSuccess();
  }, []);

  return (
    <div className="ProductDescription">
      <div className="ProductDescription__wrapper">
        <div className="ProductDescription__content">
          <img
            width={400}
            height={280}
            src={data.imageURL}
            alt="ProductDescription"
          />
        </div>
        <div className="ProductDescription__content">
          <span className="productName">{data.name}</span>
          <div className="productMain__desc" dangerouslySetInnerHTML={{ __html: data.description }}>
          </div>
          <div className="ProductDescription__button">
            <button onClick={success}>
              Как купить?
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductDescription;
